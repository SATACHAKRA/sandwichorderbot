﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Connector;

namespace Form2
{
    [BotAuthentication]
    public class MessagesController : ApiController
    {
        /// <summary>
        /// POST: api/Messages
        /// Receive a message from a user and reply to it
        /// </summary>
        internal static IDialog<SandwichOrder> MakeRootDialog()
        {
            return Chain.From(() => FormDialog.FromForm(SandwichOrder.BuildForm))
            .Do(async (context, order) =>
             {
                 try
                 {
                     var completed = await order;
                     // Actually process the sandwich order...
                     await context.PostAsync("Processed your order!");
                     await context.PostAsync("anything else I can help u with?");
                 }
                 catch (FormCanceledException<SandwichOrder> e)
                 {
                     string reply;
                     if (e.InnerException == null)
                     {
                         reply = $"You quit on {e.Last} -- maybe you can finish next time!";
                     }
                     else
                     {
                         reply = "Sorry, I've had a short circuit. Please try again.";
                     }
                     await context.PostAsync(reply);
                 }
             });
        }
        public async Task<HttpResponseMessage> Post([FromBody] Activity activity)
        {
            // Detect if this is a Message activity  
            if (activity.Type == ActivityTypes.Message)
            {
                // Call our FormFlow by calling MakeRootDialog  
                await Conversation.SendAsync(activity, MakeRootDialog);
            }
            else
            {
                // This was not a Message activity  
                HandleSystemMessageAsync(activity);
            }
            // Return response  
            var response = Request.CreateResponse(HttpStatusCode.OK);
            return response;
        }

        private async Task<Activity> HandleSystemMessageAsync(Activity message)
        {
            if (message.Type == ActivityTypes.DeleteUserData)
            {
                // Implement user deletion here
                // If we handle user deletion, return a real message
            }
            else if (message.Type == ActivityTypes.ConversationUpdate)
            {

                // Handle conversation state changes, like members being added and removed
                // Use Activity.MembersAdded and Activity.MembersRemoved and Activity.Action for info
                // Not available in all channels
                
            }
            else if (message.Type == ActivityTypes.ContactRelationUpdate)
            {
                // Handle add/remove from contact lists
                // Activity.From + Activity.Action represent what happened
            }
            else if (message.Type == ActivityTypes.Typing)
            {
                // Handle knowing tha the user is typing
            }
            else if (message.Type == ActivityTypes.Ping)
            {
            }

            return null;
        }
    }
}